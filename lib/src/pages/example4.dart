import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Example4 extends StatelessWidget {
  Example4({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];

  final image = [
    AssetImage('assets/1.jpg'),
    AssetImage('assets/2.png'),
    AssetImage('assets/3.png'),
    AssetImage('assets/4.jpg'),
    AssetImage('assets/5.png'),
    AssetImage('assets/6.png'),
    AssetImage('assets/7.jpg'),
    AssetImage('assets/8.png'),
    AssetImage('assets/9.png')
  ];

  final subtitle = [
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G',
    'H',
    'I'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView4'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: CircleAvatar(
                  backgroundImage: image[index],
                  radius: 30,
                ),
                title: Text(
                  '${titles[index]}',
                  style: TextStyle(fontSize: 18),
                ),
                subtitle: Text(
                  subtitle[index],
                  style: TextStyle(fontSize: 15),
                ),
                trailing: Icon(
                  Icons.monetization_on_outlined,
                  size: 25,
                ),
                onTap: (){
                  Fluttertoast.showToast(msg:'${titles[index]}',
                    toastLength: Toast.LENGTH_SHORT,
                  );
                },
              ),
              Divider(
                thickness: 1,
              ),
            ],
          );
        },
      ),
    );
  }
}
